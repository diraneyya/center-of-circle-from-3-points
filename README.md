# Circle-Center Online Calculator for CNC Machining

This is an online calculator that, given 3 points on the perimeter of a circle, will tell you where the center of that circle is.

**Find it here:** https://center-of-circle-from-3-points-diraneyya-4b947fda460fcf745e231d.gitlab.io/
